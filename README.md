# Apptize

## Dúvidas Frequentes

- Como preparar infra-estrutura necessaria para executar a aplicação ?

```shell
# Execute o comando Abaixo

 docker-compose -f apptize_infra_docker-compose.yml up

```

- Como executar a aplicacao ?

```shell

# Entre na pasta da aplicacao e execute o comando abaixo
docker build -t apptize-back:1.0.0 .


docker-compose -f apptize_docker-compose.yml up

```