package br.com.fiap.configuration;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "app")
public class AppConfiguration {

	@Valid
	private final Document document = new Document();

	@Getter
	@Setter
	public static class Contact {

		@NotEmpty
		private String name;

		@NotEmpty
		private String email;

		@NotEmpty
		private String url;

	}

	@Getter
	@Setter
	public static class Document {

		@NotEmpty
		private String title;

		@NotEmpty
		private String packageBase;

		@NotEmpty
		private String description;

		@NotEmpty
		private String version;

		private Contact contact;

		private String license;

		private String licenseUrl;
	}
}
