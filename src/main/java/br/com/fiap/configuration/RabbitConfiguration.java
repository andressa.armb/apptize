package br.com.fiap.configuration;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class RabbitConfiguration {

	public static final String QUEUE_LIKE_MOVIES = "process_like_movies";

	@Bean
	public Queue queue() {
		return new Queue(QUEUE_LIKE_MOVIES, true);
	}
}
