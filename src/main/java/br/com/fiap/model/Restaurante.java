package br.com.fiap.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "tb_restaurante")
@EqualsAndHashCode(callSuper = false)
public class Restaurante extends AbstractEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	private String nomeFantasia;

	@Column(nullable = false)
	private String razaoSocial;

	@Column(nullable = false)
	private String cnpj;

	@OneToMany(mappedBy = "restaurante")
	private List<Pedido> pedidos;

	@Column(nullable = false)
	private String endereco;

	@Column(nullable = false)
	private Boolean ativo;
}