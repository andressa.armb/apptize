package br.com.fiap.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "tb_cliente")
@EqualsAndHashCode(callSuper = false)
public class Cliente extends AbstractEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1L;
	@Column(nullable = false)
	private String nome;

	@Column(nullable = false)
	private String sobrenome;

	@Column(nullable = false)
	private String cpf;

	@Column(nullable = false)
	private String endereco;

	@Column(nullable = false)
	private Boolean ativo;

}