package br.com.fiap.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "tb_item_pedido")
@EqualsAndHashCode(callSuper = false)
public class ItemPedido extends AbstractEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	private long quantidade;

	@ManyToOne
	@JoinColumn(name = "cod_pedido", insertable = false, updatable = false, nullable = true)
	private Pedido pedido;

	@ManyToOne
	@JoinColumn(name = "cod_produto", insertable = false, updatable = false, nullable = true)
	private Produto produto;
}
