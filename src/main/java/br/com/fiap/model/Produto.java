package br.com.fiap.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "tb_produto")
@EqualsAndHashCode(callSuper = false)
public class Produto extends AbstractEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	private String nome;

	@Column(nullable = false)
	private Long preco;

	@Column(nullable = true)
	private String imagem;

	//	@ManyToOne
	//	@JoinColumn(name = "cod_restaurante", insertable = false, updatable = false, nullable = true)
	//	private Restaurante restaurante;

	@Column(nullable = true)
	private String restaurante;

	@Column(nullable = false)
	private Boolean ativo;
}