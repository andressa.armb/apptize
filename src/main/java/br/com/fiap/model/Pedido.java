package br.com.fiap.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "tb_pedido")
@EqualsAndHashCode(callSuper = false)
public class Pedido extends AbstractEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "cod_cliente", insertable = false, updatable = false, nullable = true)
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name = "cod_restaurante", insertable = false, updatable = false, nullable = true)
	private Restaurante restaurante;

	@OneToMany(mappedBy = "pedido")
	private List<ItemPedido> itens;

}
