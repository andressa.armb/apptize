package br.com.fiap.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fiap.model.Cliente;
import br.com.fiap.repository.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	public List<Cliente> findAll() {
		return clienteRepository.findByAtivo(true);
	}

	public Cliente find(Long id) {
		return clienteRepository.findById(id).orElse(null);
	}

	public Cliente save(Cliente cliente) {
		return clienteRepository.save(cliente);
	}

	public void delete(Long id) {
		Cliente find = clienteRepository.findById(id).orElseThrow(() -> new RuntimeException());
		find.setAtivo(false);
		clienteRepository.save(find);
	}
}
