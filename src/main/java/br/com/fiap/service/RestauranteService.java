package br.com.fiap.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fiap.model.Restaurante;
import br.com.fiap.repository.RestauranteRepository;

@Service
public class RestauranteService {

	@Autowired
	private RestauranteRepository restauranteRepository;

	public List<Restaurante> findAll() {
		return restauranteRepository.findByAtivo(true);
	}

	public Restaurante find(Long id) {
		return restauranteRepository.findById(id).orElse(null);
	}

	public Restaurante save(Restaurante restaurante) {
		return restauranteRepository.save(restaurante);
	}

	public void delete(Long id) {
		Restaurante find = restauranteRepository.findById(id).orElseThrow(() -> new RuntimeException());
		find.setAtivo(false);
		restauranteRepository.save(find);
	}
}
