package br.com.fiap.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fiap.model.Produto;
import br.com.fiap.repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;

	public List<Produto> findAll() {
		return produtoRepository.findByAtivo(true);
	}

	public Produto find(Long id) {
		return produtoRepository.findById(id).orElse(null);
	}

	public Produto save(Produto produto) {
		produto.setAtivo(true);
		return produtoRepository.save(produto);
	}

	public void delete(Long id) {
		Produto find = produtoRepository.findById(id).orElseThrow(() -> new RuntimeException());
		find.setAtivo(false);
		produtoRepository.save(find);
	}
}
