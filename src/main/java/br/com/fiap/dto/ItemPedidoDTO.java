package br.com.fiap.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ItemPedidoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private long quantidade;

	private ProdutoDTO produto;
}
