package br.com.fiap.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProdutoDTO implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String nome;

	private Long preco;

	private String imagem;

	private String restaurante;

	private Boolean ativo;

	@Override
	public Object clone() {
		try {
			// call clone in Object.
			return super.clone();
		} catch (CloneNotSupportedException e) {
			System.out.println("Cloning not allowed.");
			return this;
		}
	}
}
