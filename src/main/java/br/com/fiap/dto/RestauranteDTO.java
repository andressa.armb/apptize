package br.com.fiap.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RestauranteDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String nomeFantasia;

	private String razaoSocial;

	private String cnpj;

	private String endereco;

	private Boolean ativo;
}
