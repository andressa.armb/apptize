package br.com.fiap.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PedidoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private ClienteDTO cliente;

	private RestauranteDTO restaurante;

	private List<ItemPedidoDTO> itens;

}
