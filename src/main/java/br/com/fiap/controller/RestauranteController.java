package br.com.fiap.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fiap.dto.RestauranteDTO;
import br.com.fiap.model.Restaurante;
import br.com.fiap.service.RestauranteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags = { "Restaurante" })
@RequestMapping(path = "/restaurante")
public class RestauranteController extends AbstractController<Long, Restaurante, RestauranteDTO> {

	@Autowired
	private RestauranteService restauranteService;

	@GetMapping
	@ApiOperation(value = "Busca todos Restaurantes")
	public List<RestauranteDTO> findAll() {
		return convertToDTO(restauranteService.findAll(), RestauranteDTO.class);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Busca por Restaurante por identificador")
	public RestauranteDTO get(@ApiParam(value = "Identificador", required = true) @PathVariable final Long id) {
		return convertToDTO(restauranteService.find(id), RestauranteDTO.class);
	}

	@PostMapping
	@ApiOperation(value = "Salva Restaurante")
	public ResponseEntity<Restaurante> save(@RequestBody RestauranteDTO Restaurante) {
		Restaurante save = restauranteService.save(convertToEntity(Restaurante, Restaurante.class));
		return ResponseEntity.ok(save);
	}

	@PutMapping
	@ApiOperation(value = "Altera o Restaurante")
	public ResponseEntity<Restaurante> update(@RequestBody RestauranteDTO Restaurante) {
		Restaurante save = restauranteService.save(convertToEntity(Restaurante, Restaurante.class));
		return ResponseEntity.ok(save);
	}

}
