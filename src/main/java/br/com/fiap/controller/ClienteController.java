package br.com.fiap.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fiap.dto.ClienteDTO;
import br.com.fiap.model.Cliente;
import br.com.fiap.service.ClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags = { "Clientes" })
@RequestMapping(path = "/clientes")
public class ClienteController extends AbstractController<Long, Cliente, ClienteDTO> {

	@Autowired
	private ClienteService clienteService;

	@GetMapping
	@ApiOperation(value = "Busca todos clientes")
	public List<ClienteDTO> findAll() {
		return convertToDTO(clienteService.findAll(), ClienteDTO.class);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Busca por cliente por identificador")
	public ClienteDTO get(@ApiParam(value = "Identificador", required = true) @PathVariable final Long id) {
		return convertToDTO(clienteService.find(id), ClienteDTO.class);
	}

	@PostMapping
	@ApiOperation(value = "Salva cliente")
	public ResponseEntity<Cliente> save(@RequestBody ClienteDTO cliente) {
		Cliente save = clienteService.save(convertToEntity(cliente, Cliente.class));
		return ResponseEntity.ok(save);
	}

	@PutMapping
	@ApiOperation(value = "Altera o cliente")
	public ResponseEntity<Cliente> update(@RequestBody ClienteDTO cliente) {
		Cliente save = clienteService.save(convertToEntity(cliente, Cliente.class));
		return ResponseEntity.ok(save);
	}

	@DeleteMapping
	@ApiOperation(value = "Remove o cliente")
	public ResponseEntity<Cliente> delete(@ApiParam(value = "Identificador", required = true) @PathVariable final Long id) {
		clienteService.delete(id);
		return ResponseEntity.noContent().build();
	}
}
