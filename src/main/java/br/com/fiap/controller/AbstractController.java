package br.com.fiap.controller;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import br.com.fiap.model.AbstractEntity;

class AbstractController<I extends Serializable, E extends AbstractEntity<I>, D> {

	@Autowired
	private ModelMapper modelMapper;

	protected Page<D> convertToDTO(final Page<E> page, final Class<D> dtoClass) {
		return new PageImpl<>(page.getContent().stream().map(e -> convertToDTO(e, dtoClass)).collect(Collectors.toList()), page.getPageable(), page.getTotalElements());
	}

	protected List<D> convertToDTO(final List<E> list, final Class<D> dtoClass) {
		return list.stream().map(e -> convertToDTO(e, dtoClass)).collect(Collectors.toList());
	}

	protected D convertToDTO(final E entity, final Class<D> dtoClass) {
		return modelMapper.map(entity, dtoClass);
	}

	protected E convertToEntity(final D dto, final Class<E> entityClass) {
		return modelMapper.map(dto, entityClass);
	}

	@SuppressWarnings("null")
	protected E convertToEntity(final D dto, final I id, final Class<E> entityClass) {
		E entity = modelMapper.map(dto, entityClass);
		entity.setId(id);
		return entity;
	}
}
