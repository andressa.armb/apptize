package br.com.fiap.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fiap.dto.PedidoDTO;
import br.com.fiap.model.Pedido;
import br.com.fiap.service.PedidoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags = { "Pedido" })
@RequestMapping(path = "/pedido")
public class PedidoController extends AbstractController<Long, Pedido, PedidoDTO> {

	@Autowired
	private PedidoService pedidoService;

	@GetMapping
	@ApiOperation(value = "Busca todos Pedidos")
	public List<PedidoDTO> findAll() {
		return convertToDTO(pedidoService.findAll(), PedidoDTO.class);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Busca por Pedido por identificador")
	public PedidoDTO get(@ApiParam(value = "Identificador", required = true) @PathVariable final Long id) {
		return convertToDTO(pedidoService.find(id), PedidoDTO.class);
	}

	@PostMapping
	@ApiOperation(value = "Salva Pedido")
	public ResponseEntity<Pedido> save(@RequestBody PedidoDTO pedido) {
		Pedido save = pedidoService.save(convertToEntity(pedido, Pedido.class));
		return ResponseEntity.ok(save);
	}

	@PutMapping
	@ApiOperation(value = "Altera o Pedido")
	public ResponseEntity<Pedido> update(@RequestBody PedidoDTO Pedido) {
		Pedido save = pedidoService.save(convertToEntity(Pedido, Pedido.class));
		return ResponseEntity.ok(save);
	}

}
