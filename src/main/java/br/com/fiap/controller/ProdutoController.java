package br.com.fiap.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fiap.dto.ProdutoDTO;
import br.com.fiap.model.Cliente;
import br.com.fiap.model.Produto;
import br.com.fiap.service.ProdutoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@CrossOrigin(origins = "*")
@Api(tags = { "Produto" })
@RequestMapping(path = "/produto")
public class ProdutoController extends AbstractController<Long, Produto, ProdutoDTO> {

	@Autowired
	private ProdutoService produtoService;

	@GetMapping
	@ApiOperation(value = "Busca todos Produtos")
	public List<ProdutoDTO> findAll() {
		return convertToDTO(produtoService.findAll(), ProdutoDTO.class);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Busca por Produto por identificador")
	public ProdutoDTO get(@ApiParam(value = "Identificador", required = true) @PathVariable final Long id) {
		return convertToDTO(produtoService.find(id), ProdutoDTO.class);
	}

	@PostMapping
	@ApiOperation(value = "Salva Produto")
	public ResponseEntity<Produto> save(@RequestBody ProdutoDTO Produto) {

		//Aplicacao do Padrão Prototipe
		Object clone = Produto.clone();

		Produto save = produtoService.save(convertToEntity(Produto, Produto.class));
		return ResponseEntity.ok(save);
	}

	@PutMapping
	@ApiOperation(value = "Altera o Produto")
	public ResponseEntity<Produto> update(@RequestBody ProdutoDTO Produto) {
		
		//Aplicacao do Padrão Builder
		ProdutoDTO build = ProdutoDTO.builder()
				  .ativo(true)
				  .nome("Bolinho de Queijo")
				  .preco(10L)
				  .build();

		Produto save = produtoService.save(convertToEntity(Produto, Produto.class));
		return ResponseEntity.ok(save);
	}

	@DeleteMapping
	@ApiOperation(value = "Remove o Produto")
	public ResponseEntity<Cliente> delete(@ApiParam(value = "Identificador", required = true) @PathVariable final Long id) {
		produtoService.delete(id);
		return ResponseEntity.noContent().build();
	}
}
